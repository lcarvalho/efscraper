EF WEB SCRAPER
==============
This project provides an API to retrive profiles information from socials network.

Architecture
-----------

![architecture](https://bitbucket.org/lcarvalho/efscraper/raw/master/efarch.png)

### REQUIREMENTS (for development)
You need to have such services running on default ports:

1. MongoDB
2. Redis
3. Python2.7
4. Pip
5. Virtualenv

STRUCTURE
----------
# API

### Profile Handler
This handler can return HTTP STATUS CODE 200 or 202

	/profiles/<any(facebook, twitter):social_network>/<string:profile_id>

#### 200 HTTP CODE
The profile is already into database, so just return it

	$ curl -v -XGET http://localhost:5009/profiles/facebook/neymarjr
    > GET /profiles/facebook/neymarjr HTTP/1.1
    > User-Agent: curl/7.21.4 (universal-apple-darwin11.0) libcurl/7.21.4 OpenSSL/0.9.8y zlib/1.2.5
    > Host: localhost:5009
    > Accept: */*
    >
    * HTTP 1.0, assume close after body
    < HTTP/1.0 200 OK
    < Content-Type: application/json; charset=UTF-8
    < Content-Length: 327
    < Server: Werkzeug/0.9.6 Python/2.7.1
    < Date: Sun, 10 Aug 2014 19:05:27 GMT
    <
    {
    ¦ "image": "https://scontent-b.xx.fbcdn.net/hphotos-xpf1/v/t1.0-9/s720x720/10577106_740157422686610_1149770413070544456_n.jpg?oh=918f44cd20633702726f840d118c0d24&oe=546CD2A6",
    ¦ "name": "Neymar Jr.",
    ¦ "popularityIndex": 42708961,
    ¦ "profileId": "neymarjr",
    ¦ "shortDescription": "www.twitter.com/neymarjr",
    ¦ "socialNetwork": "facebook"
    }

#### 202 HTTP CODE
The profile is not into databse, so retreive it in background

	$ curl -v -XGET http://localhost:5009/profiles/facebook/neymarjr
    > GET /profiles/facebook/neymarjr HTTP/1.1
    > User-Agent: curl/7.21.4 (universal-apple-darwin11.0) libcurl/7.21.4 OpenSSL/0.9.8y zlib/1.2.5
    > Host: localhost:5009
    > Accept: */*
    >
    * HTTP 1.0, assume close after body
    < HTTP/1.0 202 ACCEPTED
    < Location: http://localhost:5009/profiles/status/023751d2-9d8e-43a9-bec4-7f6731f9c3b8
    < Content-Type: application/json; charset=UTF-8
    < Content-Length: 0
    < Server: Werkzeug/0.9.6 Python/2.7.1
    < Date: Sun, 10 Aug 2014 19:10:58 GMT

As you can see there is a HTTP Header called Location which should be use to check if the profile task status

### Task Handler
Based on the HTTP Header called Location from 202 Response it returns the task status

	/profiles/status/<string:task_id>

#### 200 HTTP CODE
	$ curl -v -XGET http://localhost:5009/profiles/status/023751d2-9d8e-43a9-bec4-7f6731f9c3b8
    > GET /profiles/status/023751d2-9d8e-43a9-bec4-7f6731f9c3b8 HTTP/1.1
    > User-Agent: curl/7.21.4 (universal-apple-darwin11.0) libcurl/7.21.4 OpenSSL/0.9.8y zlib/1.2.5
    > Host: localhost:5009
    > Accept: */*
    >
    * HTTP 1.0, assume close after body
    < HTTP/1.0 200 OK
    < Content-Type: application/json; charset=UTF-8
    < Content-Length: 21
    < Server: Werkzeug/0.9.6 Python/2.7.1
    < Date: Sun, 10 Aug 2014 19:19:50 GMT
    <

    {"status": "SUCCESS"}

# SCRAPER
Python package which should be installed on the API server and on Celery server.
It is responsible to save the profiles into database and to retrieve the profiles using the social network API.

### scraper - USAGE
Retrieve from database:

	$ python
	>>> from scraper import social
	>>> social.get_local_profile('facebook', 'neymarjr')
	{u'_id': ObjectId('53e7c3c4b2cc0d994714d0ad'),
 	u'image': u'https://scontent-b.xx.fbcdn.net/hphotos-xpf1/v/t1.0-9/s720x720/10577106_740157422686610_1149770413070544456_n.jpg?oh=918f44cd20633702726f840d118c0d24&oe=546CD2A6',
	u'name': u'Neymar Jr.',
 	u'popularityIndex': 42709363,
 	u'profileId': u'neymarjr',
 	u'shortDescription': u'www.twitter.com/neymarjr',
 	u'socialNetwork': u'facebook'}

Retrieve from social network API:

	$ python
	>>> from scraper import social
	>>> config = {
    'FACEBOOK_APP_ID': <FACEBOOK APP ID>
    'FACEBOOK_APP_SECRET': <FACEBOOK APP SECRET>
    'TWITTER_APP_ID': <TWITTER APP ID>
    'TWITTER_APP_SECRET': <TWITTER APP SECRET>
	}
	>>> social.get_remote_profile('facebook', 'neymarjr', config)
	{u'_id': ObjectId('53e7c3c4b2cc0d994714d0ad'),
 	u'image': u'https://scontent-b.xx.fbcdn.net/hphotos-xpf1/v/t1.0-9/s720x720/10577106_740157422686610_1149770413070544456_n.jpg?oh=918f44cd20633702726f840d118c0d24&oe=546CD2A6',
	u'name': u'Neymar Jr.',
 	u'popularityIndex': 42709363,
 	u'profileId': u'neymarjr',
 	u'shortDescription': u'www.twitter.com/neymarjr',
 	u'socialNetwork': u'facebook'}

 	>>> social.get_remote_profile.delay('facebook', 'neymarjr', config)
	<AsyncResult: 41a0d55c-f529-47fc-be4c-f2137dd0c682>


# CELERY
Asynchronous task queue/job queue configuration

# SETUP
Check Makefile helpers:

	$ make help

# TODO
1. use capstrano and puppet to deployment and setup machines
2. do not let any configuration hardcoded, use environment variabels
3. use unique constraint on mongo collection to avoid duplicate entries
4. fix duplicated profiles entries on database by tasks
5. setup Django to monitor the celery workers with report about the tasks status
6. add circuit breaker to all the requests to the APIs
7. add logs on API level, debug, info and errors
8. configure before request error handlers to return API errors as json
9. improve code expecting error, try: except:
10. write more tests :D
