# -*- encoding: utf-8 -*-
from unittest import TestCase
from pymongo import MongoClient

from scraper.db import ProfileDB

class MixinScraper(TestCase):

    maxDiff = None

    def setUp(self):
        self.profile_db = ProfileDB()
        self.profile_data = {
            u"image": u"https://scontent-b.xx.fbcdn.net/hphotos-xpf1/v/t1.0-9/s720x720/10577106_740157422686610_1149770413070544456_n.jpg?oh=918f44cd20633702726f840d118c0d24&oe=546CD2A6",
            u"name": u"Neymar Jr.",
            u"popularityIndex": 42709363,
            u"profileId": u"neymarjr",
            u"shortDescription": u"www.twitter.com/neymarjr",
            u"socialNetwork": u"facebook"
        }

    def tearDown(self):
        self.profile_db.collection.drop()
