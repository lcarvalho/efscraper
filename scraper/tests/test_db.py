# -*- encoding: utf-8 -*-
from .mixin import MixinScraper
from bson.objectid import ObjectId
from pymongo import MongoClient


class TestProfileDB(MixinScraper):

    def test_should_save_the_profile(self):
        profile_object_id = self.profile_db.save(self.profile_data)
        self.assertIsInstance(profile_object_id, ObjectId)
        self.assertEquals(1, self.profile_db.collection.count())

    def test_should_get_the_profile(self):
        profile_object_id = self.profile_db.save(self.profile_data)

        profile = self.profile_db.get('facebook', 'neymarjr')
        self.assertEquals(self.profile_data, profile)
