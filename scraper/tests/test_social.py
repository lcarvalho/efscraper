# -*- encoding: utf-8 -*-
from .mixin import MixinScraper

from mock import patch

from scraper.social import (_get_profile_class,
                            get_remote_profile,
                            get_local_profile)

from scraper.profile import FacebookProfile, TwitterProfile
from scraper.exceptions import SocialNetworkNotSupported


class TestSocial(MixinScraper):

    def setUp(self):
        super(TestSocial, self).setUp()

    def test_get_profile_class(self):
        profile_class = _get_profile_class('facebook', 'neymarjr')
        self.assertIsInstance(profile_class, FacebookProfile)

        profile_class = _get_profile_class('twitter', 'neymarjr')
        self.assertIsInstance(profile_class, TwitterProfile)

        call_get_profile_clas = lambda:  _get_profile_class('foo', 'bar')
        self.assertRaises(SocialNetworkNotSupported, call_get_profile_clas)

    def test_get_local_profile(self):
        profile_object_id = self.profile_db.save(self.profile_data)
        profile = get_local_profile('facebook', 'neymarjr')
        self.assertEquals(self.profile_data, profile)
