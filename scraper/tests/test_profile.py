# -*- encoding: utf-8 -*-
import facebook
import unittest
from .mixin import MixinScraper

from mock import patch, Mock, MagicMock, PropertyMock

from scraper.profile import Profile, FacebookProfile, TwitterProfile


class TestProfile(MixinScraper):

    def setUp(self):
        super(TestProfile, self).setUp()
        self.profile = Profile(profile_id='neymarjr')
        self.profile.social_network = 'fakesocialnetwork'

    def test_init_method_params(self):
        profile = Profile(profile_id='neymarjr', config={'foo': 'bar'})
        self.assertEquals(profile.profile_id, 'neymarjr')
        self.assertEquals(profile.config, {'foo': 'bar'})

    def test_get_profile_data_should_raise(self):
        self.assertRaises(NotImplementedError, self.profile.get_profile_data)

    def test_get_should_call_db_get(self):
        with patch.object(self.profile, 'db') as db_mock:
            foo = self.profile.get()
            self.profile.db.get.assert_called_with(self.profile.social_network, 'neymarjr')

    def test_save_should_raise(self):
        self.assertRaises(NotImplementedError, self.profile.save)


class TestFacebookProfile(MixinScraper):

    def setUp(self):
        super(TestFacebookProfile, self).setUp()
        config = {
            'FACEBOOK_APP_ID': '123456890',
            'FACEBOOK_APP_SECRET': '0987654321',
        }
        self.profile = FacebookProfile(profile_id='neymarjr', config=config)
        self.mock_response = {u'category': u'Athlete',
            u'username': u'neymarjr',
            u'talking_about_count': 4092631,
            u'description': 'Test Description',
            u'likes': 42772245,
            u'cover': {
               u'source': u'https://scontent-b.xx.fbcdn.net/hphotos-xpf1/v/t1.0-9/s720x720/10577106_740157422686610_1149770413070544456_n.jpg?oh=918f44cd20633702726f840d118c0d24&oe=546CD2A6',
               u'cover_id': 740157422686610,
               u'offset_x': 50,
               u'offset_y': 0
            },
             u'name': u'Neymar Jr.'}

    def test_get_profile_data(self):
        profile_mock = MagicMock()
        profile_mock.get_object.return_value = self.mock_response
        property_mock = Mock()
        property_mock.return_value = profile_mock
        with patch('facebook.get_app_access_token', new_callable=PropertyMock) as facebook_mock:
            with patch('facebook.GraphAPI', property_mock) as GraphAPI_mock:
                profile = self.profile.get_profile_data()
                self.assertEquals(profile['name'], self.mock_response['name'])
                self.assertEquals(profile['shortDescription'], self.mock_response['description'])
                self.assertEquals(profile['popularityIndex'], self.mock_response['likes'])
                self.assertEquals(profile['image'], self.mock_response['cover']['source'])
                self.assertEquals(profile['socialNetwork'], self.profile.social_network)
                self.assertEquals(profile['profileId'], self.profile.profile_id)


class TestTwitterProfile(MixinScraper):

    @unittest.skip("Won't implement it, cuz would be thes same approach used on TestFacebookProfile")
    def test_get_profile_data(self):
        pass
