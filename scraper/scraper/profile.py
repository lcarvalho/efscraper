# -*- encoding: utf-8 -*-
import facebook
from twython import Twython

from .db import ProfileDB


class Profile(object):

    db = ProfileDB()

    def __init__(self, profile_id, config=None):
        # TODO: should be able to load configuration from environment
        self.config = config
        self.profile_id = profile_id

    def get_profile_data(self):
        raise NotImplementedError('Must be implemented.')

    def _build_profile(self, name, short_description, popularity_index, image):
        profile = {
            'name': name,
            'shortDescription': short_description,
            'popularityIndex': popularity_index,
            'image': image,
            'socialNetwork': self.social_network,
            'profileId': self.profile_id
        }
        return profile

    def get(self):
        return self.db.get(self.social_network, self.profile_id)

    def save(self):
        profile_data = self.get_profile_data()
        profile_object_id = self.db.save(profile_data)
        return profile_object_id


class FacebookProfile(Profile):

    social_network = 'facebook'

    def get_profile_data(self):
        oauth = facebook.get_app_access_token(app_id=self.config['FACEBOOK_APP_ID'],
                                              app_secret=self.config['FACEBOOK_APP_SECRET'])
        graph = facebook.GraphAPI(oauth)
        user = graph.get_object(self.profile_id)
        image = user.get('cover').get('source') if 'cover' in user else None
        # FIXME: popularity index can not be friends count, because AFAIK,
        # it has been changed on recent Graph API and requires user oauth.
        profile_data = self._build_profile(user['name'],
                                           user.get('description', user.get('about', None)),
                                           user.get('likes', None),
                                           image)
        return profile_data


class TwitterProfile(Profile):

    social_network = 'twitter'

    def get_profile_data(self):
        twitter = Twython(self.config['TWITTER_APP_ID'],
                          self.config['TWITTER_APP_SECRET'])
        # TODO: AFAIK Twitter API also supports user_id
        user = twitter.show_user(screen_name=self.profile_id)
        profile_data = self._build_profile(user['name'],
                                           user.get('description', None),
                                           user.get('followers_count', None),
                                           user.get('profile_image_url'))
        return profile_data
