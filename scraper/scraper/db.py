# -*- encoding: utf-8 -*-
from pymongo import MongoClient

# TODO: Should not be hardcoded
# It should support configuration from environment
mongo = MongoClient()


class ProfileDB(object):

    def __init__(self):
        db = mongo['webscraper']
        self.collection = db['profile']

    def get(self, social_network, profile_id):
        profile = self.collection.find_one({'profileId': profile_id, 'socialNetwork': social_network})
        return profile

    def save(self, data):
        profile_object_id = self.collection.save(data)
        return profile_object_id
