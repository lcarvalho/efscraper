# -*- encoding: utf-8 -*-
from celery import Celery

from .profile import FacebookProfile, TwitterProfile
from .exceptions import SocialNetworkNotSupported

celery = Celery('tasks', backend='mongodb', broker='redis://localhost')


def _get_profile_class(social_network, profile_id, config=None):
    profile = None
    if social_network == 'facebook':
        profile = FacebookProfile(profile_id, config=config)
    if social_network == 'twitter':
        profile = TwitterProfile(profile_id, config=config)
    if profile is None:
        raise SocialNetworkNotSupported('%s is not supported yet.' % social_network)
    return profile


def get_local_profile(social_network, profile_id):
    profile = _get_profile_class(social_network, profile_id)
    return profile.get()


@celery.task
def get_remote_profile(social_network, profile_id, config):
    profile = _get_profile_class(social_network, profile_id, config)
    return profile.save()


def get_task_status(task_id):
    return celery.AsyncResult(task_id).state
