# -*- encoding: utf-8 -*-
from setuptools import setup, find_packages

tests_require = [
    'ipdb',
    'pytest',
    'mock'
]

setup(
    name='efscraper',
    version='0.0.1',
    description='EF Web Scraper Library',
    author='Lucas Carvalho',
    author_email='lucascarvalho@gmail.com',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'pymongo==2.7.2',
        'facebook-sdk==0.4.0',
        'twython==3.1.2',
        'celery==3.1.13',
        'redis==2.10.1'
    ],
    extras_require={
        'tests': tests_require,
    }
)
