# -*- encoding: utf-8 -*-
from flask import Flask

import settings

# Building APP
app = Flask(__name__)
app.debug = settings.DEBUG
app.config.from_object(settings)

from src.routes.root import RootMethodView
from src.routes.profile import ScraperMethodView, StatusMethodView

# + Root
app.add_url_rule('/',
                 view_func=RootMethodView.as_view('root'),
                 methods=['GET'])

# + Scrapper
app.add_url_rule('/profiles/<any(facebook, twitter):social_network>/<string:profile_id>',
                 view_func=ScraperMethodView.as_view('scraper'),
                 methods=['GET'])

app.add_url_rule('/profiles/status/<string:task_id>',
                 view_func=StatusMethodView.as_view('status'),
                 methods=['GET'])


@app.after_request
def set_content_type(response):
    response.headers['Content-Type'] = 'application/json; charset=UTF-8'
    return response
