# -*- encoding: utf-8 -*-
from flask import json, url_for
from flask.views import MethodView

from src.server import app
from scraper import social


class ScraperMethodView(MethodView):

    config = {
        'FACEBOOK_APP_ID': app.config['FACEBOOK_APP_ID'],
        'FACEBOOK_APP_SECRET': app.config['FACEBOOK_APP_SECRET'],
        'TWITTER_APP_ID': app.config['TWITTER_APP_ID'],
        'TWITTER_APP_SECRET': app.config['TWITTER_APP_SECRET']
    }

    def get(self, social_network, profile_id):
        profile = social.get_local_profile(social_network, profile_id)
        if profile:
            del profile['_id']
            return json.dumps(profile), 200

        async_task = social.get_remote_profile.delay(social_network, profile_id, self.config)
        url_status = url_for('status', task_id=async_task.task_id)
        return '', 202, {"Location": url_status}


class StatusMethodView(MethodView):

    def get(self, task_id):
        # TODO: should return the url of profile on success
        status = social.get_task_status(task_id)
        return json.dumps({'status': status}), 200
