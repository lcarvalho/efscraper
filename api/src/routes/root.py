# -*- encoding: utf-8 -*-
from flask.views import MethodView


class RootMethodView(MethodView):

    def get(self):
        return '', 200
