from flask.ext.script import Manager

from src.server import app

manager = Manager(app)


@manager.command
def runserver():
    app.run(port=5009, host='0.0.0.0')

if __name__ == "__main__":
    manager.run()
