# -*- encoding: utf-8 -*-
from unittest import TestCase
from flask import url_for

from src.server import app


class ApiTestMixin(TestCase):

    def setUp(self):
        self.fake_task_id = '023751d2-9d8e-43a9-bec4-7f6731f9c3b8'
        with app.app_context():
            self.task_status_url = url_for('status',
                                           task_id=self.fake_task_id)
        self.app = app.test_client()
