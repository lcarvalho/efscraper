# -*- encoding: utf-8 -*-
from .mixin import ApiTestMixin
from mock import patch, Mock

from flask import url_for, json

from src.server import app


class TestScraperMethodView(ApiTestMixin):

    def setUp(self):
        super(TestScraperMethodView, self).setUp()
        with app.app_context():
            self.scraper_url = url_for('scraper',
                                       social_network='facebook',
                                       profile_id='neymarjr')
        self.fake_profile = {
            '_id': '1234567890',
            'name': 'neymarjr'
        }

    def test_should_return_the_profile(self):
        with patch('scraper.social.get_local_profile', Mock()) as social_mock:
            social_mock.return_value = self.fake_profile
            response = self.app.get(self.scraper_url)
            self.assertEquals(200, response.status_code)
            self.assertEquals(self.fake_profile, json.loads(response.data))

    def test_should_return_the_202_and_task_url(self):
        with patch('scraper.social.get_local_profile', Mock()) as social_mock:
            social_mock.return_value = None
            with patch('scraper.social.get_remote_profile', Mock()) as social_remote_mock:
                social_remote_mock.delay.return_value = Mock(task_id=self.fake_task_id)
                response = self.app.get(self.scraper_url)
                self.assertEquals(202, response.status_code)
                self.assertEquals(self.task_status_url, response.headers['Location'])
                self.assertEquals('', response.data)


class TestStatusMethodView(ApiTestMixin):

    def test_should_return_the_status_of_the_task(self):
        with patch('scraper.social.get_task_status', Mock()) as social_mock:
            social_mock.return_value = 'SUCCESS'

            response = self.app.get(self.task_status_url)
            self.assertEquals(200, response.status_code)
            self.assertEquals({'status': 'SUCCESS'}, json.loads(response.data))
