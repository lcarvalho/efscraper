-r requirements.txt

# Tools
ipdb
ipython
flake8

# Tests
pytest
mock
