# -*- encoding: utf-8 -*-
from celery import Celery

from scraper import social

app = Celery('tasks', backend='mongodb', broker='redis://localhost')
